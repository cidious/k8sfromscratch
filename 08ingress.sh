cat > values.yaml <<EOF
controller:
  replicaCount: 2
  config:
    use-proxy-protocol: "true"
  service:
    annotations:
      service.beta.kubernetes.io/aws-load-balancer-proxy-protocol: '*'
EOF

helm install --name ingress --namespace ingress -f values.yaml stable/nginx-ingress

