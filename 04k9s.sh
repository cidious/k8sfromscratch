
mkdir k9s
cd k9s
wget 'https://github.com/derailed/k9s/releases/download/v0.24.2/k9s_Linux_x86_64.tar.gz'
tar xfz k9s_Linux_x86_64.tar.gz
chown root.root k9s
chmod +x k9s
mv k9s /usr/local/bin/
cd ..
rm -rf k9s
