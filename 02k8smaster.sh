#!/bin/bash

hostnamectl set-hostname kube-master

# firewall-cmd --permanent --add-port=6443/tcp
# firewall-cmd --permanent --add-port=2379-2380/tcp
# firewall-cmd --permanent --add-port=10250/tcp
# firewall-cmd --permanent --add-port=10251/tcp
# firewall-cmd --permanent --add-port=10252/tcp
# firewall-cmd --permanent --add-port=10255/tcp
# firewall-cmd --permanent --add-port=8285/tcp
# firewall-cmd --reload

systemctl stop firewalld
systemctl disable firewalld

kubeadm init --pod-network-cidr=10.244.0.0/16

echo -e '\nexport KUBECONFIG=/etc/kubernetes/admin.conf\n' >> ~/.bashrc
export KUBECONFIG=/etc/kubernetes/admin.conf

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

kubectl get nodes
