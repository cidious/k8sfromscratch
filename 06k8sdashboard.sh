helm install my-dashboard stable/kubernetes-dashboard --set rbac.clusterAdminRole=true -n dashboard --create-namespace

echo -e "\ndashboard token:\n"
kubectl -n kube-system describe $(kubectl -n kube-system get secret -n kube-system -o name | grep namespace) | grep token:
echo -e '\n'

kubectl -n dashboard get service my-dashboard-kubernetes-dashboard -o yaml | sed "s/type: ClusterIP/type: NodePort/g" | kubectl replace -f -

echo -e "\ndashboard address:\n"
kubectl get services -A | grep my-dashboard-kubernetes-dashboard | awk '{print $6}' | perl -ne 's/:(\d+)//; print "https://192.168.14.220:",$1,"\n";'

