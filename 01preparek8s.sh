#!/bin/bash

sudo -H bash

sed -i '/swap/d' /etc/fstab
swapoff -a

setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

echo -e "\n192.168.14.220 kube-master\n192.168.14.221 kube-node1\n192.168.14.222 kube-node2\n192.168.14.223 kube-node3\n" >> /etc/hosts

modprobe br_netfilter
modprobe overlay

yum check-update
yum install -y epel-release
yum install -y yum-utils device-mapper-persistent-data lvm2 mc tmux mosh htop iotop iftop nano vim wget curl iproute lsof
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker

systemctl start docker
systemctl enable docker

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum install -y kubelet kubeadm kubectl
systemctl start kubelet
systemctl enable kubelet
cat <<EOF > /etc/sysctl.d/k8s.conf

net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
