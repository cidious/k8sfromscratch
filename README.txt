﻿# качаем ISO-образ CentOS 7 отсюда:
# http://isoredirect.centos.org/centos/7/isos/x86_64/
# и устанавливаем на виртуалку.
# дальше либо задаём статический IP-адрес,
# либо прописываем MAC и конфигурируем местный DHCP чтобы он раздавал статические IP
# этим виртуалкам.
# Никаких дополнительных действий
ssh-copy-id root@192.168.14.220
ssh root@192.168.14.220 'bash -s' < 01preparek8s.sh
ssh root@192.168.14.220 'shutdown now'

# MASTERIP=192.168.14.220 ; until ping -c1 $MASTERIP >/dev/null 2>&1; do : echo "no pong from $MASTERIP so far..." ; done ; sleep 3s ; ssh-copy-id root@$MASTERIP && ssh root@$MASTERIP 'bash -s' < 01preparek8s.sh && ssh root@$MASTERIP 'shutdown now'
# enter the root password

# clone master to node1
# clone master to node2
# clone master to node3

MASTERIP=192.168.14.220 ; until ping -c1 $MASTERIP >/dev/null 2>&1; do : echo "no pong from $MASTERIP so far..." ; done ; sleep 3s ; ssh root@$MASTERIP 'bash -s' < 02k8smaster.sh
# 03k8snode.sh > kubeadm join ......
# 03k8snode.sh > hostnamectl set-hostname kube-node1
export HNAME=kube-node1 ; echo "hostnamectl set-hostname $HNAME" | cat - 03k8snode.sh | ssh root@192.168.14.221 'bash -s'
export HNAME=kube-node2 ; echo "hostnamectl set-hostname $HNAME" | cat - 03k8snode.sh | ssh root@192.168.14.222 'bash -s'
export HNAME=kube-node3 ; echo "hostnamectl set-hostname $HNAME" | cat - 03k8snode.sh | ssh root@192.168.14.223 'bash -s'

ssh root@192.168.14.220 'kubectl get nodes'

# дождаться когда у всех STATUS будет Ready

# установим k9s
# k9s это консольный менеджер k8s
# управление напоминает vim через комстроку, например список нод -- :node выход :q
# документация тут: https://k9scli.io/

ssh root@192.168.14.220 'bash -s' < 04k9s.sh


ssh root@192.168.14.220 'bash -s' < 05k8sdashboard.sh

ssh root@192.168.14.220 'bash -s' < 07helloworld.sh

